# "XSLT" TT-RSS Plugin

Before I came to TT-RSS, I had a lot of *batch* feed generators (instagram, twitter, comics, etc...) that looked like:
 * use saxon + tagsoup
 * read HTML (or XML in case of feedmorph)
 * apply XSL templates
 * run in cron (daily, weekly, etc...)
 * write new/changed feed XML to file
 * import into favorite feed aggregator

I looked at rss-bridge, feediron (great but xpath just isn't enough sometimes), and other plugins; I even wrote some plugins myself. But I really wanted to get back to the expressiveness, flexibility, power, etc... of my old XSLT scripts. Enter XSLT plugin, it ...

"Creates a feed from (usually) HTML website using XSLT. Modifying rss/atom/xml feeds are easily done also. (requires XSLT and some processors require EXSLT capability)"

## Requirements:

XSLT plugin requires, well, xslt. That is you need to install the xslt library for the OS and the xslt class for php.  If you are using the official Docker containers you simply need to run:

```bash
 % apk add --no-cache php8-xsl libxslt
```

If you ignore the official supported advice, then you need to find the right command for your OS and environment.

Here is a list and short description of the XSLT processors included in this plugin:

## Anchors:

You can subscribe to just 'anchors' (a feed item for all the a html elements with appropos link element) from websites (e.g. for CNN Photos) :
 * http://xslt.localhost/anchors?url=https://www.cnn.com/specials/photos
 * http://xslt.localhost/anchors?strip=F&url=https://www.example.com/webpage.html#fragment
 * http://xslt.localhost/anchors?strip=Q&url=https://www.example.com/webpage.html?query
 * http://xslt.localhost/anchors?strip=B&url=https://www.example.com/webpage.html?query#fragment
 * http://xslt.localhost/anchors?contains=/episode-&tcontains=Episode&strip=B&url=https://www.se-radio.net/

The anchors processor will look for text or alt attributes to place in the feed, but likely you will want to "slurp" in the actual web page.  The guid used to make articles unique is the href url.  If you supply "strip" then, for the guid only, the "F" fragment will be removed (anything past '#' including the hash mark), or the the "Q" query will be removed (anything past '?' including the question mark), or "B" both the fragment and/or query will be removed (anything past '#' or '?' including the hash mark and/or the question mark).  And an option 'contains' means the url of the anchor must contain the text in order to be included in the rss feed.  And an option 'tcontains' means the title of the anchor must contain the text in order to be included in the rss feed.

Suggested feediron pattern if you subscribe to CNN Photos (feeding anchors rss output into a second pass of feediron is a handy pattern) :

```json
"://www.cnn.com/": {
	"type": "xpath",
	"xpath": "body",
	"force_charset": "utf-8",
	"cleanup": [
		"div[contains(@class, 'share-container')]",
		"form", "iframe", "script"
	]
}
```

## Cinemark:

You can subscribe to Cinemark movie listings via (e.g. for Cinemark Tinseltown USA and IMAX in New York) :
 * http://xslt.localhost/cinemark?url=https://www.cinemark.com/theatres/ny-rochester/cinemark-tinseltown-usa-and-imax

Each movie title is a handy link to movie reviews.

## Feedmorph:

You can change Atom/RSS feeds via feedmorph (to date, this is the only XML processor) :
 * http://xslt.localhost/feedmorph?url=https://www.youtube.com/feeds/videos.xml?channel_id=UC6107grRI4m0o2-emgoDnAA
   * moves 'media:thumbnail' and 'media:description' into a 'content' element where aggregators expect to find it.
 * http://xslt.localhost/feedmorph?url=https://soylentnews.org/index.rss
   * copies 'slash:department' to 'description' so it shows in aggregators.
 * http://xslt.localhost/feedmorph?url=http://se-radio.net/feed/
   * se radio has never trimmed their feed which now has over 400 posts, feedmorph trims feeds to top 50 entries.

Presently, youtube and soylent news (and b/c codebase inheritance: slashdot) are the only morphs. Handy place to add more.

## Instagram:

You can subscribe to instagram accounts via (e.g. for NASA) :
 * http://xslt.localhost/instagram?url=https://www.instagram.com/nasa/

Note: you can log into instagram for those private feeds (ed: I think instagram requires a login on the web app whether the feed if private or not).  Copy "data/dot.appauth.php" to "data/.appauth.php" and customize for your username and password (can be per instagram feed or for *all* instagram feeds).  It would be a good idea to secure the file from world prying eyes, e.g.
Note: the "data/" folder must be writeable by the user running tt-rss (apache, httpd, nginx, app, you, etc...) to store auth cookies and "data/.appauth.php" must be readable.

```bash
chown <user>:<webserver> data data/.appauth.php
chmod 770 data
chmod 640 data/.appauth.php
```

## Stock_Ticker:

You can subscribe to stock_ticker quotes via (e.g. for 3M) :
 * http://xslt.localhost/stock_ticker?url=https://money.cnn.com/quote/quote.html?symb=MMM

## Twitter:

You can subscribe to twitter accounts via (e.g. for NASA) :
 * [1] http://xslt.localhost/twitter?url=https://twitter.com/NASA
 * [2] http://xslt.localhost/twitter?url=https://mobile.twitter.com/NASANewHorizons
 * [3] http://xslt.localhost/twitter?url=https://nitter.net/NASAJuno/rss

[1] : as of late summer 2020 twitter went to an all javascript one page webapp with api and so this url format is most likely broken forever.
[2] : the mobile version still works and is now the preferred subscription address, but only the first photo in a gallery is usually shown on mobile pages.
[3] : an alternative to mobile is to subscribe via nitter which is able to retrieve all photos from a gallery but has slightly different formatting than #2.

## Watermark:

You can subscribe to watermark sermons and/or re:gen testimonies via :
 * http://xslt.localhost/watermark?url=http://www.watermark.org/resources/messages
 * http://xslt.localhost/watermark?url=http://www.watermark.org/series/120

## Wayback:

The "wayback" processor creates an RSS feed with (back) dated items and links. A url is generated (using id, host, root, and add parameters) and put into the item/link element, then during an article_filter pass, the webpage is read and processed.

Note: there is no web page loaded on first pass, it simply creates dynamic feed, the article_filter pass will read from the web:
 * FoxTrot : http://xslt.localhost/wayback?url=http://www.gocomics.com/foxtrot/~Y/~m/~d&add=-P6139D
 * or (a slightly different "add") http://xslt.localhost/wayback?url=http://www.gocomics.com/foxtrot/~Y/~m/~d&add=-P16Y9M19D
   * On 2020/2/19 the above generates a link to 'https://www.gocomics.com/foxtrot/2003/04/30'

 * Dilbert : http://xslt.localhost/wayback?url=https://dilbert.com/strip/~Y-~m-~d&add=-P8813D

 * These are not "back dated", but provide links/content for current date:
   * C&H : http://xslt.localhost/wayback?url=http://www.gocomics.com/calvinandhobbes/~Y/~m/~d
   * FoxTrot : http://xslt.localhost/wayback?url=http://www.gocomics.com/foxtrot/~Y/~m/~d&dow=Sunday
	  * note the 'dow=Sunday' filter since current FoxTrot comics only come out on Sundays.
   * Dilbert : http://xslt.localhost/wayback?url=https://dilbert.com/strip/~Y-~m-~d
   * Sherman's Lagoon : http://xslt.localhost/wayback?url=https://www.comicskingdom.com/sherman-s-lagoon/~Y-~m-~d
	* Far Side : http://xslt.localhost/wayback?author=Gary+Larson&url=https://www.thefarside.com/~Y/~m/~d

'add=-P1D' means jump backwards 1 day, so wayback will create links w/ yesterday's date (ref. 'Duration Data Type' at https://www.w3schools.com/xml/schema_dtypes_date.asp)

## TODO:

 * All configuration per subscription is in the subscribe url; easy but maybe not elegant and maybe not in the spirit of TT-RSS plugins. Move to prefs screen?
 * Create a test suite.
 * Improve (i.e. create) documentation
