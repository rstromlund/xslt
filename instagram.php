<?php

require_once __DIR__ . '/data/.appauth.php';
if (! isset($profile_auths) || count($profile_auths) < 1 || ! isset($profile_auths['instagram.ALL'])) {
	Logger::log_error(E_USER_ERROR, "Instagram profile authorizations not setup properly.  See 'data/dot.appauth.php' for details.", __FILE__, __LINE__, "");
}

class Instagram extends Xslt_Processor {

	const LOGIN_URL = 'https://www.instagram.com/accounts/login/ajax/';
	private $iprofile = null;

	protected function get_profile() {
		global $profile_auths;
		if (! isset($this->iprofile)) {
			$this->iprofile = 'instagram.' . explode('/', $this->config_info['url'], 5)[3]; // e.g. https://www.instagram.com/tmahlmann/
			if (! isset($profile_auths[$this->iprofile])) {
				$profile_auths[$this->iprofile = 'instagram.ALL'];
			}
		}

		return $this->iprofile;
	}

	function fetch_url($url) {
		global $profile_auths;
		$auth = $profile_auths[$this->get_profile()];
		_debug("profile = $url {$this->iprofile}, auth? " . (isset($auth) ? "T" : "F"));

		$curl_http_headers = [
			'Connection: keep-alive',
			'Cache-Control: max-age=0',
			'Authority: www.instagram.com',
			'Upgrade-Insecure-Requests: 1',
			'Host: www.instagram.com',
			'Origin: https://www.instagram.com',
			'X-IG-WWW-Claim: 0',
			'Sec-Fetch-Site: same-origin',
			'Sec-Fetch-Mode: cors',
			'Sec-Fetch-Dest: empty',
			'x-ig-app-id: 936619743392459'
		];

		[$h, $t] = $this->fetch_with_cookies($url, $curl_http_headers);
		// _debug('instagram::fetch_with_cookies: h = ' . str_replace('&', '~', str_replace('<', '_', $h)));
		// _debug('instagram::fetch_with_cookies: t = ' . str_replace('&', '~', str_replace('<', '_', $t)));

		if (strpos($h, 'ocation:') !== false) {
			preg_match('/\nLocation:\s*(\S+)/si', $h, $matches);
			_debug("NOTE 302 redirect found, probably challenging you -- " . htmlspecialchars($h));
			Logger::log_error(E_USER_WARNING, "NOTE 302 redirect found, probably challenging you -- " . htmlspecialchars($h), __FILE__, __LINE__, "");

			if (count($matches) >= 1) { [$h, $t] = $this->fetch_with_cookies($matches[1], $curl_http_headers); }
		}

		// Maybe we need to be logged in?
		if ($auth && (strpos($t, 'not-logged-in') !== false) || ((strpos($h, 'ocation:') !== false && strpos($h, '/login') !== false)))
		{
			_debug('Attempting to login.');

			array_push($curl_http_headers, 'Content-Type: application/x-www-form-urlencoded');

			preg_match('/\nSet-Cookie:\s*csrftoken=([^;]*)/si', $h, $matches);
			if (count($matches) >= 1) { array_push($curl_http_headers, "X-CSRFToken: $matches[1]"); }

			preg_match('/"rollout_hash":"([^"]*)"/s', $t, $matches);
			if (count($matches) >= 1) { array_push($curl_http_headers, "X-Instagram-AJAX: $matches[1]"); }

			// [$h, $li] = $this->fetch_with_gzip($ch, $curl_http_headers);
			[$h, $li] = $this->fetch_with_cookies(
				Instagram::LOGIN_URL, $curl_http_headers,
				"username=$auth[id]&enc_password=#PWD_INSTAGRAM_BROWSER:0:" . time() . ":$auth[pwd]&optIntoOneTap=false"
			);


			if (false !== strpos($li, 'checkpoint_required')) {
				_debug("NOTE manual login required, probably wants to send an email -- " . htmlspecialchars($li));
				Logger::log_error(E_USER_WARNING, "NOTE manual login required, probably wants to send an email -- " . htmlspecialchars($li), __FILE__, __LINE__, "");
				return '<html />';

			}

			else if (false === strpos($li, 'authenticated": true') && false === strpos($li, 'authenticated":true')) {
				_debug("ERROR failed to login -- " . htmlspecialchars($li));
				Xslt_Processor::reportErrors();
				Logger::log_error(E_USER_WARNING, "Failed to login -- " . htmlspecialchars($li) . " -- $h -- " . json_encode($curl_http_headers), __FILE__, __LINE__, "");
				return(false);

			}

			_debug('Successfully logged in.');
			[$h, $t] = $this->fetch_with_cookies($url, $curl_http_headers);

		}

		return [$h, $t];
	}

}
