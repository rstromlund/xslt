<?php

require_once __DIR__ . "/xslt_processor.php";

class Xslt extends Plugin {

	const host_trigger = "xslt.localhost";
	private static $proc_cache = [];

	static function getXslt($path) {
		$xslt = Xslt::$proc_cache[$path] ?? null;
		if (isset($xslt)) {
			return $xslt;
		}

		if (file_exists(__DIR__ . "$path.php")) {
			require_once __DIR__ . "$path.php";
			$class = ucfirst(substr($path, 1));
		} else {
		  $class = 'Xslt_Processor';
		}

		_debug("Xslt::getXslt: class=$class");
		return (Xslt::$proc_cache[$path] = new $class());
	}


	function about() {
		return [1.0, // version
			'Creates a feed from (usually) HTML website using XSLT. Modifying rss/atom/xml feeds are easily done also. (requires XSLT and some processors require EXSLT capability)', // descr
			'rodneys_mission', // author
			false, // is_system
			'https://gitlab.com/rstromlund/xslt' // more info
		];
	}

	function init($host) {
		// Set priority = 49, just under the default.  Should get us before plugins like 'feediron' so they can process our output.
		$host->add_hook($host::HOOK_FETCH_FEED, $this, 49);
		$host->add_hook($host::HOOK_FEED_BASIC_INFO, $this, 49);
		$host->add_hook($host::HOOK_SUBSCRIBE_FEED, $this, 49);
		$host->add_hook($host::HOOK_PREFS_TAB, $this, 49);
		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this, 49);
	}

	function gen_feed($fetch_url, $feed) {
		$fetch_comp = parse_url($fetch_url);
		// _debug("xslt/init.php: $fetch_comp[host],$fetch_comp[path],$fetch_comp[query]");

		if (self::host_trigger === $fetch_comp['host']) {
			parse_str($fetch_comp['query'], $config_info);

			$xslt = Xslt::getXslt($fetch_comp['path']);
			$xslt->init($fetch_comp, $config_info);
			return $xslt->process();

		}

		_debug("gen_feed: Unrecognized url for xslt plugin : $fetch_url");
		return false;

	}

	/**
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	function hook_fetch_feed($feed_data, $fetch_url, $owner_uid, $feed, $last_article_timestamp, $auth_login, $auth_pass) {
		if (preg_match('#^https?://'.self::host_trigger.'/([-a-z0-9_]+)\?#i', $fetch_url)) {
			$feed_data = $this->gen_feed($fetch_url, $feed);
		}

		return $feed_data;
	}

	function hook_subscribe_feed($contents, $url, $auth_login, $auth_pass) {
		if (preg_match('#^https?://'.self::host_trigger.'/([-a-z0-9_]+)\?#i', $url))
			return '<?xml version="1.0" encoding="utf-8"?>'; // Make is_html() return false

		return $contents;
	}

	function hook_feed_basic_info($basic_info, $fetch_url, $owner_uid, $feed, $auth_login, $auth_pass) {
		if (preg_match('#^https?://'.self::host_trigger.'/([-a-z0-9_]+)\?#i', $fetch_url, $matches)) {
			// Get the basic info from the feed, since it is templated it could be anything.

			$rss = new FeedParser($this->gen_feed($fetch_url, $feed));
			$rss->init();
			if (!$rss->error()) {
				$basic_info = ['title' => $rss->get_title(), 'site_url' => $rss->get_link()];
			}
		}

		return $basic_info;
	}

	function hook_article_filter($article) {
		$fetch_url = $article['feed']['fetch_url'];
		$fetch_comp = parse_url($fetch_url);

		if (self::host_trigger !== $fetch_comp['host']) {
			return($article); // Only 'xslt' classes can have a 'article_filter' method.
		}

		parse_str($fetch_comp['query'], $config_info);

		$xslt = Xslt::getXslt($fetch_comp['path']);
		$xslt->init($fetch_comp, $config_info);
		return $xslt->article_filter($article);

	}

	function hook_prefs_tab($args) {
		if ($args != 'prefFeeds') return;

		print '<div dojoType="dijit.layout.AccordionPane" title="<i class=\'material-icons\'>rss_feed</i> '.__('Feeds supported by xslt').'">';

		print "<p>" . __("The following Processors are currently supported:") . "</p>";

		print "<ul class='panel panel-scrollable list list-unstyled'>";
		print array_reduce(
			// remove 'library.xsl' and 'static.xsl' from filename list, they are not xslt classes/processors (they are libraries and tests).
			array_filter(glob(__DIR__ . '/xsl/*.xsl'), function ($fn) {
				return(0 !== substr_compare($fn, 'library.xsl', -11) && 0 !== substr_compare($fn, 'static.xsl', -10));
			}),

			// build html list items for each remaining XSL filename.
			function ($carry, $fn) {$c = basename($fn, '.xsl'); return "$carry<li>$c</li>"; }
		);
		print "</ul>";

		print '<p>'.__('Use url <code>http://'.self::host_trigger.'/{processor}?{config_parms}*</code> to subscribe.').'</p>';

		print '<p>'.__('E.g.<ul><li>to read NASA\'s <em>Instagram</em> feed <code>http://'.self::host_trigger.'/instagram?url=https://www.instagram.com/nasa/</code></li>').'</p>';
		print '<p>'.__('<li>to read NASA\'s <em>Twitter</em> feed <code>http://'.self::host_trigger.'/twitter?url=https://twitter.com/NASA</code></li>').'</p>';
		print '<p>'.__('<li>to modify <em>SoylentNews</em>, or <em>Slashdot</em>, or <em>Youtube RSS</em> feed <ul><li><code>http://'.self::host_trigger.'/feedmorph?url=https://soylentnews.org/index.rss</code></li><li><code>http://'.self::host_trigger.'/feedmorph?url=http://rss.slashdot.org/Slashdot/slashdot</code></li><li><code>http://'.self::host_trigger.'/feedmorph?url=https://www.youtube.com/feeds/videos.xml?channel_id=UC6107grRI4m0o2-emgoDnAA</code></li></ul>').'</p>';
		print '<p>'.__('<li>A few of the author\'s favorites:<ul><li>FathomMag links (used w/ feediron): <code>http://'.self::host_trigger.'/anchors?url=https://www.fathommag.com</code></li><li>3M stock quotes: <code>http://'.self::host_trigger.'/stock_ticker?url=https://money.cnn.com/quote/quote.html?symb=MMM</code></li><li>Re:generation testimonies: <code>http://'.self::host_trigger.'/watermark?url=http://www.watermark.org/series/120</code></li><li>Watermark sermons: <code>http://'.self::host_trigger.'/watermark?url=http://www.watermark.org/resources/messages</code></li><li>Cinemark movie listings: <code>http://'.self::host_trigger.'/cinemark?url=https://www.cinemark.com/new-york/cinemark-tinseltown-usa-and-imax</code></li><li>Calvin And Hobbes: <code>http://'.self::host_trigger.'/wayback?url=http://www.gocomics.com/calvinandhobbes/~Y/~m/~d</code></li><li>Dilbert classics: <code>http://'.self::host_trigger.'/wayback?url=https://dilbert.com/strip/~Y-~m-~d&add=-P8813D</code></li><li>Dilbert: <code>http://'.self::host_trigger.'/wayback?url=https://dilbert.com/strip/~Y-~m-~d</code></li><li>FoxTrot (Sunday only): <code>http://'.self::host_trigger.'/wayback?url=http://www.gocomics.com/foxtrot/~Y/~m/~d&dow=Sunday</code></li><li>Peanuts classic: <code>http://'.self::host_trigger.'/wayback?url=http://www.gocomics.com/peanuts/~Y/~m/~d&add=-P23814D</code></li><li>Pearls Before Swine: <code>http://'.self::host_trigger.'/wayback?url=http://www.gocomics.com/calvinandhobbes/~Y/~m/~d</code></li><li>Sherman\'s Lagoon: <code>http://'.self::host_trigger.'/wayback?url=https://www.comicskingdom.com/sherman-s-lagoon/~Y-~m-~d</code></li><li>The Far Side: <code>http://'.self::host_trigger.'/wayback?author=Gary+Larson&url=https://www.thefarside.com/~Y/~m/~d</code></li></ul></li></ul>').'</p>';

		print '</div>';
	}

	function api_version() {
		return 2;
	}

}
