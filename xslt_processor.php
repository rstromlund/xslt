<?php

const UA = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; Trident/7.0; rv:11.0) like Gecko';


class Xslt_Processor {

	protected $fetch_comp;
	protected $config_info;
	protected $uniqid;

	private static $xsl_cache = [];
	private static $this_cache = [];


	static function reportErrors() {
		foreach (libxml_get_errors() as $error) {
			_debug("reportErrors: ERR=$error->message");
		}
	}

	// Calls coming back in from xsltproc don't have a pointer, give them one via the uniqid in "this_cache".
	static function getProcessorInstance($u) {
		_debug("getProcessorInstance: uniqid=$u");
		// _debug("getProcessorInstance: this_cache=" . print_r(Xslt_Processor::$this_cache, TRUE));
		return Xslt_Processor::$this_cache[$u];
	}

	static function getXslt($path) {
		list($filt, $proc) = Xslt_Processor::$xsl_cache[$path] ?? null;
		if (isset($proc)) {
			_debug("getXslt: reusing xslt stylesheet $path");
			return $proc;
		}

		_debug("getXslt: creating xslt stylesheet $path");

		$xsl = new DOMDocument();
		$uri = __DIR__ . "/xsl$path.xsl";
		$sgml = file_get_contents($uri);
		// _debug('getXslt: sgml = ' . str_replace('&', '~', str_replace('<', '_', $sgml)));

		if (! $xsl->loadXML($sgml)) { _debug("getXslt: XSLT load failed for -- $path"); return; }
		$xsl->documentURI = $uri;

		// a template matching "article-filter" means this is *article-filter* aware.
		$filt = (false !== strpos($sgml, 'match="article-filter"'));

		$proc = new XSLTProcessor();
		// _debug("getXslt: hasExsltSupport [$uri] -- " . $proc->hasExsltSupport());
		$proc->registerPHPFunctions(['json2dom', 'html2dom', 'url2dom']);

		libxml_clear_errors();
		if (! $proc->importStylesheet($xsl)) {
			_debug("getXslt: Failed to 'importStylesheet' -- $path");
			Xslt_Processor::reportErrors();
			return;
		}

		_debug("getXslt: done -- $path");
		Xslt_Processor::$xsl_cache[$path] = [$filt, $proc];
		return $proc;
	}


	function init($fetch_comp, $config_info) {
		$this->fetch_comp  = $fetch_comp;
		$this->config_info = $config_info;

		Xslt_Processor::$this_cache[$this->uniqid = uniqid($fetch_comp['path'] . '.rand.' . random_int(PHP_INT_MIN, PHP_INT_MAX) . ':', true)] = $this;
		_debug("Xslt_Processor::init: uniqid=$this->uniqid");
	}


	function article_filter($article) {
		list($filt, $proc) = Xslt_Processor::$xsl_cache[$this->fetch_comp['path']];
		if (! $filt) {
			// article filtering not requested
			return $article;
		}

		_debug('article_filter: title=' . $article['title']);

		$url = $article['link'];
		$sgml = $article['content'];

		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		libxml_use_internal_errors(true);
		$doc->loadXML("<article-filter>$sgml</article-filter>");

		$txt = $this->process_doc($doc, $url);
		if (! $txt) return $article;

		// _debug('article_filter: XSLT returns = ' . str_replace('&', '~', str_replace('<', '_', $txt)));

		if ('' !== $txt && $txt !== $article['content']) {
			// Crude way to use XML, but I just want to grab a couple things w/o a full parse.  :/
			// Note: this makes it imperative you have <link/> before <description/>.

			preg_match("~<item>.*?<link>([^<>]*)</link>.*?<description>(.*?)</description>~su", $txt, $elems);

			if ($elems && $elems[1]) $article['link'] = $elems[1];
			if ($elems && $elems[2]) $article['content'] = $elems[2];

			preg_match_all("~<category>([^<>]*)</category>~su", $txt, $elems);
			if ($elems && sizeof($elems[1]) > 0) {
				$article['tags'] = $elems[1];
			}

		}

		return $article;

	}


	function process() {
		return $this->process_url($this->config_info['url'], '#default');
	}


	function fetch_url($url) {
		// Good spot to overload and perform auth functions.
		return $this->fetch_with_cookies($url);
	}


	function process_url($url) {
		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		libxml_use_internal_errors(true);

		if (FALSE !== strpos($url, '~')) {
			// $url is "parameterized", don't retrieve a real document. In this case the XSLT processor doesn't really need/want
			// a document, it will form the url and retrieve the document itself.

			_debug("process_url: fake root for parameterized url = $url");
			$doc->loadXML('<root />');

		} else {
			$sgml = $this->fetch_url($url)[1];
			// _debug('process_url: sgml = ' . str_replace('&', '~', str_replace('<', '_', $sgml)));
			if (! isset($sgml) || '' == $sgml) $sgml = '<empty />';

			// Useful for feeds w/ non-CDATA protected HTML entities in XML (tsk, tsk):
			if (isset($this->config_info['entity_decode'])) {_debug('entity_decode'); $sgml = html_entity_decode($sgml);}

			// Useful for feeds that have non-valid UTF8:
			if (isset($this->config_info['validate_utf8'])) {_debug('validate_utf8'); $sgml = mb_convert_encoding($sgml, 'UTF-8', 'UTF-8');}

			if (preg_match('/^.{1,200}<html/si', $sgml)) {
				// In the first 200-ish characters we found an opening HTML tag; treat as HTML
				$doc->loadHTML('<?xml encoding="UTF-8">' . $sgml);

			} else {
				// Failing that, treat as XML
				$doc->loadXML($sgml);

			}

		}

		return $this->process_doc($doc, $url);
	}


	function process_doc($doc, $url) {
		$proc = Xslt_Processor::getXslt($this->fetch_comp['path']);

		// Reset some common params so they don't bleed over
		// TODO: look for top level 'xsl:param'-es and clear parameters smartly and dynamically.
		$proc->setParameter('', 'author', '');
		$proc->setParameter('', 'dow', '');
		$proc->setParameter('', 'add', '');
		$proc->setParameter('', 'strip', '');
		$proc->setParameter('', 'contains', '');
		$proc->setParameter('', 'tcontains', '');
		$proc->setParameter('', 'cdata-description', '');

		foreach ($this->config_info as $k => $v) {
			$proc->setParameter('', $k, $v);
		}
		$proc->setParameter('', 'url', $url);
		$proc->setParameter('', 'uniqid', $this->uniqid);

		// _debug('process_doc: Source=' . str_replace('&', '~', str_replace('<', '_', $doc->saveXML())));

		$res = $proc->transformToXML($doc);
		if (! $res) {
			_debug('process_doc: tranform failed!');
			Xslt_Processor::reportErrors();
		}

		// _debug('process_doc: XSLT returns = ' . str_replace('&', '~', str_replace('<', '_', $res)));

		if ('1' === ($this->config_info['cdata-description'] ?? '')) {
			// Wrap <description/> elements in CDATA sections; push on thru html errors :|
			$res = str_replace('<description>', '<description><![CDATA[', str_replace('</description>', ']]></description>', $res));
		}

		return $res;
	}


	protected function fetch_with_gzip($ch) {
		$e = @curl_exec($ch);
		// _debug('fetch_with_gzip: e=' . print_r($e, TRUE));

		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$hdr = substr($e, 0, $header_size);
		$resp = substr($e, $header_size);

		if ("\x1F" . "\x8B" . "\x08" == substr($resp, 0, 3)) {
			# Response was gzipped (some sites block user agents who cannot handle compression).
			$resp = gzdecode($resp);
		}

		return([$hdr, $resp]);
	}


	// get_profile: fetch_with_cookies uses this to create cookie files, overloadable for auth functions.
	protected function get_profile() {return substr($this->fetch_comp['path'], 1);}

	public function fetch_with_cookies($url, $curl_http_headers = [], $post_data = '') {
		$profile = $this->get_profile();
		_debug("fetch_with_cookies: [$profile] $url");

		// Initialize cURL
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_VERBOSE, '1'); //useful at times.

		curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . "/data/.cookie-$profile.jar");
		curl_setopt($ch, CURLOPT_COOKIEJAR,  __DIR__ . "/data/.cookie-$profile.jar");

		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_USERAGENT, UA);
		if (0 === strncmp(getenv('HOSTNAME'), 'SY4-', 4)) curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Ignore cert errors? (@ work: yes b/c middleware).

		// array_push($curl_http_headers, 'X-Requested-With: XMLHttpRequest');
		array_push($curl_http_headers, 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9');
		array_push($curl_http_headers, 'Accept-Encoding: gzip, identity'); # some sites fail w/o this header and allowing gzip :/
		array_push($curl_http_headers, 'Accept-Language: en-US,en;q=0.9');

		// _debug('fetch_with_cookies: curl_http_headers=' . print_r($curl_http_headers, TRUE));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_http_headers);
		curl_setopt($ch, CURLOPT_REFERER, $url);

		curl_setopt($ch, CURLOPT_URL, $url);

		if ('' !== $post_data) {
			// _debug("fetch_with_cookies: set post data=$post_data");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		}

		[$h, $t] = $this->fetch_with_gzip($ch);
		curl_close($ch);

		if (false === strpos(substr($h, 0, 20), '200')) {
			_debug("fetch_with_cookies: Not 200 -- $url : hcode => $h");
		}

		// _debug("fetch_with_cookies: $url => $h");
		// _debug("fetch_with_cookies: ($url) returns=" . str_replace('&', '~', str_replace('<', '_', $t)));
		return([$h, $t]);

	}

}


// ================================================================================
//		Helper functions callable from xslt via php namespace.
// ================================================================================

function _json2elem($e, $j) {

	foreach ($j as $k => $v) {
		if (is_array($v)) {
			if (is_numeric(substr($k, 0, 1))) {
				// _debug("_json2elem:createElement:1: item_$k");
				_json2elem($e->appendChild($e->ownerDocument->createElement("item_$k")), $v);
			}
			else {
				// _debug("_json2elem:createElement:2: " . preg_replace('/^(\d)/', 'n_$1', $k));
				_json2elem($e->appendChild($e->ownerDocument->createElement(preg_replace('/^(\d)/', 'n_$1', $k))), $v);
			}
		}
		else {
			$_v = 'boolean' === gettype($v) ? ($v ? "true" : "false") : str_replace('&', '&amp;', $v);

			if (is_numeric(substr($k, 0, 1))) {
				$e->setAttribute("_$k", $_v);
			}
			else {
				try {
					// _debug("_json2elem:createElement:3: " . str_replace('/', '.', $k));
					$e->appendChild($e->ownerDocument->createElement(str_replace('/', '.', $k), $_v));
				} catch (Exception $e) {
					_debug('Caught exception: ' . $e->getMessage() . ' :: ' . $k . ' = ' . $_v);
				}

			}

		}
	}

	return $e;

}


// ** Take JSON as input, return DOMDocument. Suitable for xslt template processing.

function json2dom($u, $s) {
	if (is_array($s)) {$s = $s[0];}
	if ($s instanceof DOMDocument || $s instanceof DOMElement) {$s = $s->textContent;}

	// _debug("json2dom: Trying to parse=$s");

	$doc = new DOMDocument;
	_json2elem($doc->appendChild($doc->createElement('root')), json_decode($s, true));

	// _debug("json2dom: returning=" . str_replace('&', '~', str_replace('<', '_', $doc->saveXML())));
	return $doc;

}


// ** Take HTML (tagsoup) as input, return DOMDocument. Suitable for xslt template processing.

function html2dom($u, $s) {
	if (is_array($s)) {$s = $s[0];}
	if ($s instanceof DOMDocument || $s instanceof DOMElement) {$s = $s->textContent;}

	// _debug("html2dom: Trying to parse=$s");

	$doc = new DOMDocument;
	$doc->loadHTML('<?xml encoding="UTF-8">' . $s);

	// _debug("html2dom: returning=" . str_replace('&', '~', str_replace('<', '_', $doc->saveXML())));
	return $doc;

}


// ** Take URL as input (can be XML or (possibly malformed) HTML), return DOMDocument. Suitable for xslt template processing.

function url2dom($u, $s) {
	_debug("url2dom: Trying to load url=$s for $u");

	[$hdrs, $txt] = Xslt_Processor::getProcessorInstance($u)->fetch_url($s);
	// _debug("url2dom: hdrs.content-type = $hdrs");

	if (false !== stripos($hdrs, 'Content-Type: application/json')) {
		$doc = json2dom($u, html_entity_decode($txt, ENT_NOQUOTES, 'utf-8'));
		// _debug('url2dom: (json2dom) returns = ' . str_replace('&', '~', str_replace('<', '_', $doc->saveXML())));
		return $doc;

	} else {
		$doc = new DOMDocument;
		$doc->loadHTML('<?xml encoding="UTF-8">' . $txt);
		// _debug('url2dom: (loadHTML) returns = ' . str_replace('&', '~', str_replace('<', '_', $doc->saveXML())));
		return $doc;

	}

}
