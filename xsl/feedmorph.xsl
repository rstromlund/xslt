<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:rss="http://purl.org/rss/1.0/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	xmlns:media="http://search.yahoo.com/mrss/"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />


<xsl:param name="tcontains" select="''" />


<xsl:template match="@*|node()">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()" />
	</xsl:copy>
</xsl:template>


<!-- Youtube: move thumbnail and description into a "content" element. -->

<xsl:template match="atom:entry" xmlns="http://www.w3.org/2005/Atom">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()" />
		<content type="xhtml">
			<div>
				<xsl:for-each select="media:group/media:thumbnail"><p><img src="{@url}" alt="Video thumbnail" /></p></xsl:for-each>
				<xsl:for-each select="media:group/media:description"><p><xsl:copy-of select="node()" /></p></xsl:for-each>
			</div>
		</content>
	</xsl:copy>
</xsl:template>


<!-- TT-RSS 'FeedItem_Atom::get_content' incorrectly pulls this as the entry/content (hiding the above work).  So just elide it. -->
<xsl:template match="media:content" />


<!-- Soylent/Slashdot: Copy the department name into the description (the website includes the dept name in the readable text, why doesnt the rss?) -->
<xsl:variable name="byline"><![CDATA[<p class="byline">]]></xsl:variable>

<xsl:template match="rss:description[parent::rss:item]" xmlns="http://purl.org/rss/1.0/">
	<xsl:variable name="intro" select="substring-before(., $byline)" />
	<xsl:variable name="outtro" select="substring-after(., $byline)" />

	<xsl:copy>
		<xsl:value-of select="$intro" />
		<xsl:if test="$outtro"><xsl:value-of select="$byline" /></xsl:if>

		<xsl:for-each select="../slash:department">
			<xsl:text>from the &lt;em&gt;</xsl:text>
			<xsl:value-of select="." />
			<xsl:text>&lt;/em&gt; dept. </xsl:text>
		</xsl:for-each>

		<xsl:choose>
			<xsl:when test="$outtro"><xsl:value-of select="$outtro" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
		</xsl:choose>
	</xsl:copy>
</xsl:template>


<!-- Nitter/Twitter: put author and timestamp into the title (stop huge text wrapping headlines) -->
<xsl:template match="title[parent::item]" xmlns="http://purl.org/rss/1.0/">
	<xsl:copy>
		<xsl:choose>
			<xsl:when test="'y' = $tcontains">
				<xsl:value-of select="translate(../dc:creator, '@', '')" />
				<xsl:text> @ </xsl:text>
				<xsl:value-of select="../pubDate" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:copy>
</xsl:template>


<!-- SE-Radio never trims their RSS feed and so aggregators can never purge articles. :/  50 articles is more than enough for RSS feeds (the ones I subscribe to anyway). -->
<xsl:template match="item[count(preceding-sibling::item) >= 50]" />


</xsl:stylesheet>
