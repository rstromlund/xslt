<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:php="http://php.net/xsl"
	xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	exclude-result-prefixes="php str date"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />

<xsl:param name="url" select="'http://e.g.url/'" />
<xsl:param name="uniqid" select="'Uniquely identifies this processor when calling back into php.'" />


<xsl:template name="resolve-uri">
	<xsl:param name="href" />
	<xsl:variable name="toks" select="str:tokenize($url, '/')" />
		<!-- Toks: 1=http: 2=www.wayback.com 3=path1 4=path2 5=... -->
	<xsl:variable name="base-href" select="concat($toks[1], '//', $toks[2])" />

	<xsl:choose>
		<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
		<xsl:when test="starts-with($href, '/')"><xsl:value-of select="concat($base-href, $href)" /></xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="'/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, $href)" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($url, '/', $href)" /></xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- This should only happen when filtering a twitter article, read tweet page to get full list of images/videos -->

<xsl:template match="article-filter">
	<xsl:if test="contains($url, '/status/') and (contains($url, 'twitter.com/') or contains($url, 'nitter.'))">
		<xsl:apply-templates select="php:function('url2dom', $uniqid, $url)" />
	</xsl:if>
</xsl:template>


<!-- Filter nitter.net rss feeds and enhance a bit. -->

<xsl:template match="rss">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()" mode="identity" />
	</xsl:copy>
</xsl:template>


<!-- Do our best to create a twitter feed from the mobile html. -->

<xsl:template match="html">
	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<link><xsl:value-of select="$url" /></link>
			<xsl:apply-templates />
		</channel>
	</rss>
</xsl:template>


<xsl:template match="title[parent::head]">
	<title><xsl:value-of select="." /></title>
	<description><xsl:value-of select="." /></description>
</xsl:template>


<xsl:template match="body">
	<xsl:apply-templates select=".//img[contains(@class, 'ProfileAvatar-image') and @src][1]|.//td['avatar' = @class][1]/img" mode="logo" />
	<xsl:apply-templates />
</xsl:template>


<!-- Classic: img class="ProfileAvatar-image " src="https://pbs.twimg.com/profile_images/978824564267298816/BzGkDkPY_400x400.jpg" alt="Smarter Every Day" -->
<!-- Mobile: td class="avatar"><img alt="NASA New Horizons" src="https://pbs.twimg.com/profile_images/700391254018932736/uv8wosXg_normal.jpg"></td -->

<xsl:template match="img" mode="logo" >
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="@src" /></xsl:call-template></xsl:variable>

	<image>
		<url><xsl:value-of select="$href" /></url>
		<title><xsl:value-of select="$url" /><xsl:text> | Twitter</xsl:text></title>
		<link><xsl:value-of select="$url" /></link>
	</image>
</xsl:template>


<!-- Classic: -->

<xsl:template match="div['content' = @class]">
	<xsl:variable name="permalink-ts" select="(.//a[contains(@class, 'permalink')])[1]" />
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="$permalink-ts/a/@href" /></xsl:call-template></xsl:variable>
	<xsl:variable name="time" select="$permalink-ts/@title" />

	<xsl:variable name="author" select=".//div['stream-item-header' = @class]/a[1]//*[contains(@class, 'fullname')]" />
	<xsl:variable name="creator" select=".//span[contains(@class, 'username')]/b/text()" />

	<item>
		<guid><xsl:value-of select="$href" /></guid>
		<link><xsl:value-of select="$href" /></link>
		<title><xsl:value-of select="$author" /><xsl:text> @ </xsl:text><xsl:value-of select="$time" /></title>
		<dc:creator><xsl:choose><xsl:when test="$creator"><xsl:value-of select="$creator" /></xsl:when><xsl:otherwise><xsl:value-of select="substring-after($url, '.com/')" /></xsl:otherwise></xsl:choose></dc:creator>

		<description>
			<xsl:apply-templates mode="identity" />
		</description>
	</item>
</xsl:template>


<xsl:template match="div[starts-with(@class, 'QuoteTweet') and not(starts-with(@class, 'QuoteTweet-'))]" mode="identity">
	<blockquote><xsl:apply-templates mode="identity" /></blockquote>
</xsl:template>


<xsl:variable name="sQ">'</xsl:variable>

<xsl:template match="div[contains(@style, 'background-image:url(')]" mode="identity">
	<!-- This 'src' variable will contain its own quotes, none added. -->
	<xsl:variable name="img-url" select="substring-before(substring-after(@style, 'background-image:url('), ')')" />

	<div>
		<em>media thumbnail:</em>
		<br/>
		<img src="{translate($img-url, $sQ, '')}" />
	</div>
</xsl:template>


<xsl:template match="s|span|div[not(img)]" priority="0.4" mode="identity">
	<!-- element 's' surrounds the '@' character, why?  JS trigger?  Too many empty 'div's, clean some of them up. (css hangars) -->
	<xsl:apply-templates mode="identity" />
</xsl:template>


<xsl:template match="text()[not(normalize-space(.))]" mode="identity">
	<xsl:text> </xsl:text>
</xsl:template>


<xsl:template match="@*|node()" mode="identity">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()" mode="identity" />
	</xsl:copy>
</xsl:template>


<xsl:template match="comment()|@shape" mode="identity" />


<xsl:template match="text()" mode="identity">
	<xsl:variable name="txt" select="normalize-space(str:replace(translate(., '&#xA0;&#8206;&#8207;', ' '), '&#10;', '&lt;br/>'))" />
	<xsl:variable name="ps" select="str:split($txt, '&lt;br/>')" />

	<xsl:for-each select="$ps">
		<xsl:if test="position() > 1"><br/></xsl:if>
		<xsl:value-of select="." />
	</xsl:for-each>

	<xsl:if test=". != $txt">
		<!-- We modified the text, pad to stop run-on text -->
		<xsl:text> </xsl:text>
	</xsl:if>
</xsl:template>


<!-- Mobile: -->

<xsl:template match="table[(starts-with(@class, 'tweet') and ancestor::body[contains(@class, 'users-page')]) or (starts-with(@class, 'main-tweet'))]|div['main-tweet' = @class]">
	<xsl:variable name="permalink-ts" select="(.//td['timestamp' = @class]|.//span['tweet-date' = @class])[1]" />
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="$permalink-ts/a/@href" /></xsl:call-template></xsl:variable>
	<xsl:variable name="time" select="($permalink-ts/self::span['tweet-date' = @class]/a/@title|$permalink-ts/a/text())[1]" />

	<xsl:variable name="author" select="(.//strong['fullname' = @class]/text()|.//div['fullname' = @class]//strong/text()|.//a['username' = @class]/text())[1]" />
	<xsl:variable name="creator" select="normalize-space(.//div['username' = @class]/text()[normalize-space(.)]|.//span['username' = @class]/text()[normalize-space(.)])" />

	<item>
		<guid><xsl:value-of select="str:replace($href, '//twitter.com', '//mobile.twitter.com')" /></guid>
		<link><xsl:value-of select="str:replace($href, '//twitter.com', '//mobile.twitter.com')" /></link>
		<title>
			<xsl:value-of select="$author" />
			<xsl:text> @ </xsl:text>
			<xsl:value-of select="$time" />
			<xsl:text> [</xsl:text>
			<xsl:value-of select="substring-after($href, '/status/')" />
			<xsl:text>]</xsl:text>
		</title>
		<dc:creator><xsl:value-of select="$author" /></dc:creator>

		<description>
			<xsl:for-each select="ancestor::body[contains(@class, 'tweets-page')]//div[contains(@class, 'inreplytos')]/table[contains(@class, 'tweet')]">
				<p>In reply to:</p>
				<xsl:apply-templates mode="identity" />
				<hr/>
			</xsl:for-each>

			<xsl:apply-templates mode="identity" />
		</description>
	</item>
</xsl:template>


<!-- To: https://i.ytimg.com/vi/kWKXvLMrjEU/hqdefault.jpg
From: a href="https://t.co/K5oq4omMbr" rel="nofollow noopener" dir="ltr" data-expanded-url="https://youtu.be/kWKXvLMrjEU" data-url="https://youtu.be/kWKXvLMrjEU" class="twitter_external_link dir-ltr tco-link" target="_blank" title="https://youtu.be/kWKXvLMrjEU">youtu.be/kWKXvLMrjEU</a -->

<xsl:template match="a[contains(@data-expanded-url, 'https://youtu')]" mode="identity">
	<xsl:variable name="vid-id" select="substring-after(substring(@data-expanded-url, 9, 99), '/')" />
	<xsl:variable name="img-url" select="concat('https://i.ytimg.com/vi/', $vid-id, '/hqdefault.jpg')" />

	<div>
		<em>media thumbnail:</em>
		<br/>
		<a>
			<xsl:apply-templates select="@*" mode="identity" />
			<img alt="media thumbnail:" src="{translate($img-url, $sQ, '')}" />
		</a>
	</div>
</xsl:template>


<!-- nitter youtube vids:
	<a href="https://invidious.snopyta.org/kWKXvLMrjEU">invidious.snopyta.org/kWKXvLMrjEU</a>
	https://invidious.snopyta.org/watch?v=WCvfX5xUHas&feature=emb_title
-->

<xsl:template match="a[contains(@href, 'https://invidious.snopyta.org/')]" mode="identity">
	<xsl:variable name="vid-id_">
		<xsl:choose>
			<xsl:when test="contains(@href, 'watch?v=')"><xsl:value-of select="substring-after(@href, 'watch?v=')" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="substring-after(substring(@href, 9, 99), '/')" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="vid-id">
		<xsl:choose>
			<xsl:when test="contains($vid-id_, '&amp;')"><xsl:value-of select="substring-before($vid-id_, '&amp;')" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="$vid-id_" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="img-url" select="concat('https://i.ytimg.com/vi/', $vid-id, '/hqdefault.jpg')" />
	<div>
		<em>media thumbnail:</em>
		<br/>
		<a href="https://www.youtube.com/watch?v={$vid-id}">
			<img alt="media thumbnail:" src="{translate($img-url, $sQ, '')}" />
		</a>
	</div>
</xsl:template>


<xsl:template match="a[contains(@data-url, '/video') or contains(@data-expanded-path, '/video')]" mode="identity">
	<xsl:text>[</xsl:text>
	<em>media thumbnail:</em>
	<xsl:text>] </xsl:text>

	<xsl:copy>
		<xsl:apply-templates select="@*|node()" mode="identity" />
	</xsl:copy>
</xsl:template>


<xsl:template match="@src|@href" mode="identity">
	<xsl:variable name="src"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="." /></xsl:call-template></xsl:variable>

	<xsl:attribute name="{name()}">
		<xsl:value-of select="str:replace(str:replace(str:replace($src, '3Dsmall', '3Dorig'), ':small', ''), 'http://', 'https://')" />
	</xsl:attribute>
</xsl:template>


<!-- Skip tags, include children: -->

<xsl:template match="table|thead|tbody|tr|td" mode="identity">
	<xsl:apply-templates mode="identity" />
</xsl:template>


<!-- nitter: process html in description to add thumbnails and such. -->

<xsl:template match="description[parent::item]" mode="identity">
	<xsl:copy>
		<xsl:apply-templates select="php:function('html2dom', $uniqid, .)/html/body/*" mode="identity" />
	</xsl:copy>
</xsl:template>


<xsl:template match="title[parent::item]" mode="identity">
	<xsl:variable name="title-txt" select="substring(normalize-space(.), 1, 90)" />

	<xsl:copy>
		<title>
			<xsl:value-of select="../dc:creator" />
			<xsl:text> @ </xsl:text>
			<xsl:value-of select="../pubDate" />
			<xsl:text> [</xsl:text>
			<xsl:value-of select="substring-before(substring-after(../guid, '/status/'), '#')" />
			<xsl:text>] </xsl:text>
		</title>
	</xsl:copy>
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="text()|div[contains(@class,'replies')]" />
<xsl:template mode="identity" match="ul|button|script|iframe|td['action' = @class or 'action-last' = @class or 'meta-and-actions' = @class]|a[span[contains(@class, 'verified')] or img[contains(@alt, 'Verified Account')]]|*['u-hiddenVisually' = @class]|div['stream-item-footer' = @class or 'self-thread-context' = @class or contains(@class, 'popup-tagged-users')]" />


</xsl:stylesheet>
