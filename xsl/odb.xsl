<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	exclude-result-prefixes="php str date"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />

<xsl:param name="url" select="'http://e.g.url/'" />
<xsl:param name="uniqid" select="'Uniquely identifies this processor when calling back into php.'" />


<xsl:template match="html">
	<xsl:variable name="now" select="date:date-time()" />
	<xsl:variable name="year" select="date:year($now)" />
	<xsl:variable name="month" select="str:align(date:month-in-year($now), '00', 'right')" />
	<xsl:variable name="day" select="str:align(date:day-in-month($now), '00', 'right')" />

	<rss version="2.0">
		<channel>
			<title>Our Daily Bread Podcast | Our Daily Bread</title>
			<description>Daily Devotionals</description>
			<copyright>&#xAE; &amp; &#xA9; 2021 Our Daily Bread Ministries</copyright>
			<language>en-us</language>

			<link><xsl:value-of select="$url" /></link>
			<image>
				<url><xsl:value-of select="//img[not(preceding-sibling::img)]/@src" /></url>
				<title><xsl:value-of select="$url" /><xsl:text> | Odb</xsl:text></title>
				<link><xsl:value-of select="$url" /></link>
			</image>

			<item>
				<xsl:variable name="odb-json" select="php:function('url2dom', $uniqid, concat('https://api.experience.odb.org/devotionals?on=', $month, '/', $day, '/', $year, '&amp;site_id=1&amp;country=US'))" />
				<xsl:variable name="img-url" select="$odb-json//*[self::image_url or self::email_image_url][1]" />

				<title><xsl:value-of select="$odb-json//title" /></title>
				<link><xsl:value-of select="concat('https://odb.org/', $year, '/', $month, '/', $day, '/')" /></link>
				<enclosure url="{$odb-json//audio_url}" length="" type="audio/mpeg" />
				<guid><xsl:value-of select="concat('https://odb.org/', $year, '/', $month, '/', $day, '/')" /></guid>
				<description>
					<img src="{$img-url}" class="today-img" alt="Today&apos;s Devotional" />
					<xsl:apply-templates select="$odb-json" mode="json" />
					<!-- DEBUG:<dl><xsl:for-each select="/root/default/item_0/image_url"><dt><xsl:value-of select="name()" /></dt><dd><xsl:value-of select="." /></dd></xsl:for-each></dl> -->
				</description>
				<creator xmlns="http://purl.org/dc/elements/1.1/"><xsl:value-of select="$odb-json//author_name" /></creator>
			</item>
		</channel>
	</rss>
</xsl:template>


<xsl:template match="root|default|*[starts-with(name(), 'item')]" mode="json">
	<xsl:apply-templates mode="json" />
</xsl:template>


<xsl:template match="title" mode="json">
	<h1><xsl:value-of select="text()" disable-output-escaping="yes" /></h1>
</xsl:template>


<xsl:template match="bible_in_a_year_references" mode="json">
	<p>
		<strong>Bible in a Year:</strong>
		<xsl:text> </xsl:text>
		<a href="{../bible_in_a_year_url}"><xsl:value-of select="." /></a>
	</p>
</xsl:template>


<xsl:template match="verse" mode="json">
	<p>
		<strong>Verse:</strong>
		<xsl:text> </xsl:text>
		<xsl:value-of select="str:replace(text(), 'smallcaps', 'em')" disable-output-escaping="yes" />
	</p>
</xsl:template>


<xsl:template match="content" mode="json">
	<xsl:value-of select="text()" disable-output-escaping="yes" />
</xsl:template>


<xsl:template match="insights" mode="json">
	<p>
		<strong>Insights:</strong>
		<xsl:text> </xsl:text>
		<xsl:value-of select="text()" disable-output-escaping="yes" />
	</p>
</xsl:template>


<xsl:template match="passage_reference" mode="json">
	<p>
		<strong>Today's Scripture &amp; Insight:</strong>
		<xsl:text> </xsl:text>
		<a href="{../passage_url}"><xsl:value-of select="." /></a>
	</p>
</xsl:template>


<xsl:template match="response" mode="json">
	<p>
		<strong>Reflect &amp; Pray:</strong>
		<xsl:text> </xsl:text>
		<xsl:value-of select="text()" disable-output-escaping="yes" />
	</p>
</xsl:template>


<xsl:template match="thought" mode="json">
	<p>
		<strong>Thought:</strong>
		<xsl:text> </xsl:text>
		<xsl:value-of select="text()" disable-output-escaping="yes" />
	</p>
</xsl:template>


<xsl:template match="*" mode="json" priority="-2">
	<p class="debug">more elements: <xsl:value-of select="name()" />=<xsl:value-of select="." /></p>
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="text()" />

<xsl:template mode="json" match="site_id|status|date|author|bible_in_a_year|categories|excerpt|meta|author_name|slug|insights_author|audio_url|US|tags|
	insight_question|brand_logo_url|brandName|date_id|insights_author_name|language|preheader_text|youversionLink|youversionVerseRef|youversionBiay|bible_in_a_year_url|
	lang_author_name|author_link|insights_author_id|shareable_image|image_url|email_image_url|sku_listname|passage|passage_url
" />


</xsl:stylesheet>
