<?xml version="1.0" encoding="utf-8"?>

<xsl:transform
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:func="http://exslt.org/functions"
	xmlns:lib="http://127.0.0.1/functions"
	extension-element-prefixes="func lib"
>

<!--
	FIXME: abandoning a "common" library of templates and functions (for now); php or windows or cygwin or apache isn't cooperating!  :-(
-->


<!-- It is possible to make a 'resolve-uri' template and remove the 'exslt' dependency. -->

<!-- xsl:template name="resolve-uri">
	<xsl:param name="href" />
	<xsl:variable name="toks" select="str:tokenize($url, '/')" />
		<! - - Toks: 1=http: 2=www.wayback.com 3=path1 4=path2 5=... - ->
	<xsl:variable name="base-href" select="concat($toks[1], '//', $toks[2])" />

	<xsl:choose>
		<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
		<xsl:when test="starts-with($href, '/')"><xsl:value-of select="concat($base-href, $href)" /></xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="'/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, $href)" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($url, '/', $href)" /></xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template -->


<func:function name="lib:resolve-uri">
	<xsl:param name="url" />
	<xsl:param name="href" />

	<!-- This is very basic right now. See 'rewrite_relative_url' in functions.php if more use cases are needed. -->
	<!-- TODO: possibly register 'rewrite_relative_url' as accessible via 'php:*' function call.  Overhead? Performance? -->

	<func:result>
		<xsl:choose>
			<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
			<xsl:when test="starts-with($href, '/') and '/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, substring-after($href, '/'))" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="concat($url, $href)" /></xsl:otherwise>
		</xsl:choose>
	</func:result>
</func:function>


</xsl:transform>
