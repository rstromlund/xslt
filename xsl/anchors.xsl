<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:str="http://exslt.org/strings"
	exclude-result-prefixes="str"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />


<xsl:param name="url" select="'http://e.g.url/'" />
<!-- F = fragment (# and following); Q=query (? and following); and B=both -->
<xsl:param name="strip" select="''" />
<!-- if set, href must contain the string to be included in the rss feed. -->
<xsl:param name="contains" select="''" />
<xsl:param name="tcontains" select="''" />


<xsl:template name="resolve-uri">
	<xsl:param name="href" />
	<xsl:variable name="toks" select="str:tokenize($url, '/')" />
		<!-- Toks: 1=http: 2=www.wayback.com 3=path1 4=path2 5=... -->
	<xsl:variable name="base-href" select="concat($toks[1], '//', $toks[2])" />

	<xsl:choose>
		<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
		<xsl:when test="starts-with($href, '/')"><xsl:value-of select="concat($base-href, $href)" /></xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="'/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, $href)" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($url, '/', $href)" /></xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="html">
	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<link><xsl:value-of select="$url" /></link>
			<description><xsl:value-of select="$url" /> | Anchors</description>
			<xsl:apply-templates />
		</channel>
	</rss>
</xsl:template>


<xsl:template match="title[parent::head]">
	<title><xsl:value-of select="normalize-space(.)" /></title>
</xsl:template>


<xsl:template match="link[parent::head and 'icon' = @rel]">
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="@href" /></xsl:call-template></xsl:variable>

	<image>
		<url><xsl:value-of select="$href" /></url>
		<title><xsl:value-of select="$url" /> | Anchors Icon</title>
		<link><xsl:value-of select="$url" /></link>
	</image>
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="
	a['/' = @href
		or '' = @href
		or '/login' = @href
		or starts-with(@href, '#')
		or starts-with(@href, 'mailto:')
		or contains(@href, 'itunes')
		or contains(@href, 'instagram')
		or contains(@href, 'twitter')
		or contains(@href, 'facebook')]
	|div['thumbnail' = @class or 'story-readlink' = @class]
	|text()"
/>


<xsl:template match="a">
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="@href" /></xsl:call-template></xsl:variable>
	<xsl:variable name="title">
		<xsl:choose><xsl:when test="not(normalize-space(.))"><xsl:value-of select="$href" /></xsl:when>
		<xsl:otherwise><xsl:value-of select="normalize-space(.)" /></xsl:otherwise></xsl:choose>
	</xsl:variable>
	<xsl:variable name="guid">
		<xsl:choose>
			<xsl:when test="'T' = $strip">
				<!-- Use anchor text (w/o timestamp elements) as GUID -->
				<xsl:value-of select="concat('http://localhost/dummy/a-text?', translate(normalize-space(str:concat(.//text()[not(parent::*[contains(@class, 'imestamp')])])), ' &#xA0;', '--'))" />
			</xsl:when>

			<xsl:otherwise>
				<!-- Strip either the Fragment or the Query or Both -->
				<xsl:variable name="href_stripF">
					<xsl:choose>
						<xsl:when test="('F' = $strip or 'B' = $strip) and contains($href, '#')"><xsl:value-of select="substring-before($href, '#')" /></xsl:when>
						<xsl:otherwise><xsl:value-of select="$href" /></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="('Q' = $strip or 'B' = $strip) and contains($href_stripF, '?')"><xsl:value-of select="substring-before($href_stripF, '?')" /></xsl:when>
					<xsl:otherwise><xsl:value-of select="$href_stripF" /></xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:if test="$href != $url and concat($href, '/') != $url and (not($contains) or contains($href, $contains)) and (not($tcontains) or contains($title, $tcontains))">
		<item>
			<guid><xsl:value-of select="$guid" /></guid>
			<link><xsl:value-of select="$href" /></link>
			<title><xsl:value-of select="$title" /></title>

			<!-- This is specifically for 'fathommag.com' method of indicating authors -->
			<xsl:for-each select="parent::h3/following-sibling::div[@data-authorname]/@data-authorname">
				<dc:creator><xsl:value-of select="." /></dc:creator>
			</xsl:for-each>

			<description>
				<xsl:value-of select="$title" />
				<xsl:text> [</xsl:text>
				<xsl:value-of select="$href" />
				<xsl:text>]</xsl:text>
			</description>
		</item>
	</xsl:if>
</xsl:template>


</xsl:stylesheet>
