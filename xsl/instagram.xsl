<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:php="http://php.net/xsl"
	xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	exclude-result-prefixes="php str date"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />

<xsl:param name="url" select="'http://e.g.url/'" />
<xsl:param name="uniqid" select="'Uniquely identifies this processor when calling back into php.'" />
<xsl:param name="api-count" select="'12'" />


<xsl:template name="resolve-uri">
	<xsl:param name="href" />
	<xsl:variable name="toks" select="str:tokenize($url, '/')" />
		<!-- Toks: 1=http: 2=www.wayback.com 3=path1 4=path2 5=... -->
	<xsl:variable name="base-href" select="concat($toks[1], '//', $toks[2])" />

	<xsl:choose>
		<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
		<xsl:when test="starts-with($href, '/')"><xsl:value-of select="concat($base-href, $href)" /></xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="'/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, $href)" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($url, '/', $href)" /></xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="html">
	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<link><xsl:value-of select="$url" /></link>
			<xsl:apply-templates />
		</channel>
	</rss>
</xsl:template>


<xsl:template match="title[parent::head]">
	<title><xsl:value-of select="normalize-space(.)" /></title>
	<description><xsl:value-of select="normalize-space(.)" /></description>
</xsl:template>


<!-- meta property="og:image" content="https://scontent-msp1-1.cdninstagram.com/v/t51.2885-19/11264652_1621014408186909_516878473_a.jpg..." / -->

<xsl:template match="meta[parent::head and 'og:image' = @property and @content]">
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="@content" /></xsl:call-template></xsl:variable>

	<image>
		<url><xsl:value-of select="$href" /></url>
		<title><xsl:value-of select="$url" /><xsl:text> | Instagram</xsl:text></title>
		<link><xsl:value-of select="$url" /></link>
	</image>
</xsl:template>


<!-- link rel="canonical" href="https://www.instagram.com/nasa/" -->
<!-- User page data now comes from an api, get username and load json data -->

<xsl:template match="link['canonical' = @rel and not(contains(@href, '/p/'))]">
	<xsl:variable name="toks" select="str:tokenize(@href, '/')" />
	<xsl:variable name="user" select="$toks[3]" />
	<xsl:variable name="api-url" select="concat('https://www.instagram.com/api/v1/feed/user/', $user, '/username/?count=', $api-count)" />

	<xsl:apply-templates select="php:function('url2dom', $uniqid, $api-url)" mode="homepage" />
</xsl:template>


<xsl:template match="items[parent::root]" mode="homepage">
	<xsl:for-each select="*[starts-with(name(), 'item')]">
		<item>
			<xsl:apply-templates mode="mk_entry" />
		</item>
	</xsl:for-each>
</xsl:template>


<xsl:template match="taken_at" mode="mk_entry">
	<title>
		<xsl:value-of select="parent::*/user/username" />
		<xsl:text> @ </xsl:text>
		<xsl:value-of select="date:add('1970-01-01T00:00:00Z', concat('PT', ., 'S'))" />
	</title>
	<dc:creator><xsl:value-of select="parent::*/user/full_name" /></dc:creator>
</xsl:template>


<xsl:template match="code" mode="mk_entry">
	<xsl:variable name="href" select="concat('https://www.instagram.com/p/', ., '/')" />

	<guid><xsl:value-of select="$href" /></guid>
	<link><xsl:value-of select="$href" /></link>
</xsl:template>


<xsl:template match="caption" mode="mk_entry">
	<description>
		<div>
			<xsl:apply-templates mode="mk_entry" />

			<xsl:for-each select="parent::*/image_versions2/candidates/*[1]/url">
				<br />
				<xsl:if test="parent::*/parent::candidates/parent::image_versions2/parent::*/video_versions"><em>media thumbnail:</em><br/></xsl:if>
				<img src="{.}" />
			</xsl:for-each>

			<xsl:for-each select="parent::*/carousel_media/*">
				<br />
				<xsl:if test="parent::*/video_versions"><em>media thumbnail:</em><br/></xsl:if>
				<img src="{image_versions2/candidates/*[1]/url}" />
			</xsl:for-each>
		</div>
	</description>
</xsl:template>


<xsl:template match="text[parent::caption]" mode="mk_entry">
	<!-- We will "wrap" the text node without using a <pre/> element, replace newline with <br/>. Looks close to original and better than pre-text. -->

	<xsl:for-each select="str:tokenize(translate(., '&#x2060;&#x2800;&#x2063;', '   '), '&#x0A;')">
		<xsl:if test="normalize-space(.) and not('.' = normalize-space(.))">
			<!-- Was this 1 newline or more?  str:tokenize slurps up all the delimiters so we dont know. [put in 2 for a clean break, seems to fit IG look.] -->
			<xsl:if test="position() > 1"><br /><br /></xsl:if>
			<xsl:value-of select="." />
		</xsl:if>
	</xsl:for-each>
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="text()" />
<xsl:template match="text()" mode="homepage" />
<xsl:template match="text()" mode="mk_entry" />


</xsl:stylesheet>
