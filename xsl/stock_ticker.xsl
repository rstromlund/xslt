<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:str="http://exslt.org/strings"
	xmlns:vars="http://127.0.0.1/vars"
	exclude-result-prefixes="date str vars"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />


<xsl:param name="url" select="'http://e.g.url/'" />
<xsl:param name="mkt-open" select="9" />
<xsl:param name="mkt-close" select="16" />

<xsl:variable name="now" select="date:date-time()" /> <!-- e.g. 2020-02-17T14:34:23-06:00 -->
<xsl:variable name="symbol" select="substring-before(/html/head/title, ' ')" />


<xsl:template name="format-pubDate">
	<xsl:param name="dt" />

	<xsl:value-of select="substring(date:day-name($dt), 1, 3)" />
	<xsl:text>, </xsl:text>
	<xsl:value-of select="str:align(date:day-in-month($dt), '00', 'right')" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring(date:month-name($dt), 1, 3)" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="str:align(date:year($dt), '0000', 'right')" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring(date:time($dt), 1, 8)" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="translate(substring(date:time($dt), 9), ' :', '')" />
</xsl:template>


<vars:holidays>
	<!-- New Years Day -->
	<vars:day value="2019-01-01" />
	<vars:day value="2020-01-01" />
	<vars:day value="2021-01-01" />

	<!-- MLK Day -->
	<vars:day value="2019-01-21" />
	<vars:day value="2020-01-20" />
	<vars:day value="2021-01-18" />

	<!-- Washington's Birthday -->
	<vars:day value="2019-02-18" />
	<vars:day value="2020-02-17" />
	<vars:day value="2021-02-15" />

	<!-- Good Friday -->
	<vars:day value="2019-04-19" />
	<vars:day value="2020-04-10" />
	<vars:day value="2021-04-02" />

	<!-- Memorial Day -->
	<vars:day value="2019-05-27" />
	<vars:day value="2020-05-25" />
	<vars:day value="2021-05-31" />

	<!-- Independence Day -->
	<vars:day value="2019-07-04" />
	<vars:day value="2020-07-03" />
	<vars:day value="2021-07-05" />

	<!-- Labor Day -->
	<vars:day value="2019-09-02" />
	<vars:day value="2020-09-07" />
	<vars:day value="2021-09-06" />

	<!-- Thanksgiving Day -->
	<vars:day value="2019-11-28" />
	<vars:day value="2020-11-26" />
	<vars:day value="2021-11-25" />

	<!-- Christmas -->
	<vars:day value="2019-12-25" />
	<vars:day value="2020-12-25" />
	<vars:day value="2021-12-24" />

	<!-- test -->
	<vars:day value="2020-02-19" />
</vars:holidays>


<xsl:template match="html">
	<xsl:variable name="dt" select="substring($now,  1, 10)" />
	<xsl:variable name="tm" select="substring($now, 12,  5)" />

	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<image>
				<url><xsl:text>https://money.cnn.com/favicon.ico</xsl:text></url>
				<title><xsl:text>money.cnn.com favicon</xsl:text></title>
				<link><xsl:text>https://money.cnn.com/data/markets/</xsl:text></link>
			</image>

			<xsl:choose>
				<xsl:when test="1 = date:day-in-week($now) or 7 = date:day-in-week($now)">
					<!-- No Markets on weekends (1=Sunday, 7=Saturday) -->
				</xsl:when>
				<xsl:when test="$mkt-open > date:hour-in-day($now) or date:hour-in-day($now) > $mkt-close">
					<!-- Markets closed -->
				</xsl:when>
				<xsl:when test="document('')/*/vars:holidays/vars:day[$dt = @value]">
					<!-- Market holiday -->
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
				</xsl:otherwise>
			</xsl:choose>
		</channel>
	</rss>
</xsl:template>


<xsl:template match="title[parent::head]">
	<title><xsl:value-of select="." /></title>
	<description><xsl:value-of select="." /></description>
</xsl:template>


<!-- div class="mod-quoteinfo" -->

<xsl:template match="div['mod-quoteinfo' = @class]">
	<xsl:variable name="company" select=".//h1[1]" />
	<xsl:variable name="price" select=".//td['wsod_last' = @class]/span" />
	<xsl:variable name="time">
		<xsl:choose>
			<xsl:when test=".//div['wsod_quoteLabelAsOf' = @class]/span">
				<xsl:value-of select="substring-before(.//div['wsod_quoteLabelAsOf' = @class]/span, ' ')" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring-before(.//div['wsod_quoteLabelAsOf' = @class], ' ')" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<item>
		<guid><xsl:value-of select="$url" />/<xsl:value-of select="$symbol" />/<xsl:value-of select="concat(substring($now, 1, 10), 'T', $time)" /></guid>
		<link><xsl:text>http://money.cnn.com/quote/quote.html?symb=</xsl:text><xsl:value-of select="$symbol" /></link>
		<title>
			<xsl:value-of select="$symbol" />
			<xsl:text> - </xsl:text>
			<xsl:value-of select="$price" />
			<xsl:text> - </xsl:text>
			<xsl:value-of select="$time" />
			<xsl:text> - </xsl:text>
			<xsl:value-of select="$company" />
			<xsl:text> - CNNMoney.com</xsl:text>
		</title>
		<dc:creator><xsl:value-of select="$symbol" /><xsl:text> (stock-ticker bot)</xsl:text></dc:creator>
		<pubDate>
			<xsl:call-template name="format-pubDate"><xsl:with-param name="dt" select="$now" /></xsl:call-template>
		</pubDate>

		<description>
			<xsl:apply-templates mode="identity" />
		</description>
	</item>
</xsl:template>


<xsl:template match="img[contains(@src, 'file.png')]" mode="identity" />


<xsl:template match="@*|node()" mode="identity">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()" mode="identity" />
	</xsl:copy>
</xsl:template>


<xsl:template match="text()[not(normalize-space(.))]" mode="identity">
	<xsl:text> </xsl:text>
</xsl:template>


<xsl:template match="text()" mode="identity">
	<xsl:value-of select="translate(., '&#xA0;&#8206;&#8207;', ' ')" />
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="text()" />
<xsl:template match="div['wsod_fRight' = @class]|ul['wsod_quoteNav' = @id]|div['wsod_chartControls' = @id]|form|iframe|script|comment()|@shape|@style|@*[starts-with(., 'javascript:')]" mode="identity" />


</xsl:stylesheet>
