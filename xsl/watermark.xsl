<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:php="http://php.net/xsl"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:str="http://exslt.org/strings"
	xmlns:vars="http://127.0.0.1/vars"
	exclude-result-prefixes="php date str vars"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />

<xsl:param name="url" select="'http://e.g.url/'" />
<xsl:param name="uniqid" select="'Uniquely identifies this processor when calling back into php.'" />


<xsl:template name="format-pubDate">
	<xsl:param name="dt" />

	<xsl:value-of select="substring(date:day-name($dt), 1, 3)" />
	<xsl:text>, </xsl:text>
	<xsl:value-of select="str:align(date:day-in-month($dt), '00', 'right')" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring(date:month-name($dt), 1, 3)" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="str:align(date:year($dt), '0000', 'right')" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring(date:time($dt), 1, 8)" />
	<xsl:text> </xsl:text>
	<xsl:value-of select="translate(substring(date:time($dt), 9), ' :', '')" />
</xsl:template>


<xsl:template name="resolve-uri">
	<xsl:param name="href" />
	<xsl:variable name="toks" select="str:tokenize($url, '/')" />
		<!-- Toks: 1=http: 2=www.wayback.com 3=path1 4=path2 5=... -->
	<xsl:variable name="base-href" select="concat($toks[1], '//', $toks[2])" />

	<xsl:choose>
		<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
		<xsl:when test="starts-with($href, '/')"><xsl:value-of select="concat($base-href, $href)" /></xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="'/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, $href)" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($url, '/', $href)" /></xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="article-filter">
	<!-- Just enough RSS to get the data back into 'xslt_processor.php' -->
	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0"><channel><item>

		<xsl:choose>
			<xsl:when test="not(contains(., 'testimony ')) or contains($url, '.mp3')">
				<!-- Not a testimony feed or feed already knows the audio link -->
				<xsl:copy-of select="*" />
			</xsl:when>

			<xsl:otherwise>
				<!--
					 A testimony's audio is in yet another page w/ embeded play instructions in a div; e.g.:
					 <div class='js-jwplayer' data-jwplayer='{"autostart":false,"playlist":[{"gaTitle":"Kristin - People Pleasing (7214)","mediaid":null,"file":"https://s3.amazonaws.com/media-files.watermark.org/assets/20200120/0f4a2713-1107-4f08-9c03-25418565608c/2020-01-20-Kristin.mp3"}],"skin":{"name":"watermark"},"volume":100}'></div>
				-->

				<xsl:variable name="id" select="substring-before(substring-after(., '/message/'), '-')" />
				<xsl:variable name="href" select="concat('https://media.watermark.org/embeds/', $id, '/audio?theme=watermark')" />

				<xsl:variable name="embed" select="php:function('url2dom', $uniqid, $href)" />
				<xsl:variable name="media" select="concat('http', substring-before(substring-after($embed//div[contains(@data-jwplayer, '.mp3')]/@data-jwplayer, 'http'), '.mp3'), '.mp3')" />

				<link><xsl:value-of select="$media" /></link>
				<description><xsl:copy-of select="*|text()" /></description>
			</xsl:otherwise>
		</xsl:choose>

	</item></channel></rss>
</xsl:template>


<xsl:template match="html">
	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<link><xsl:value-of select="$url" /></link>
			<language>en</language>
			<copyright>Watermark Community Church</copyright>
			<ttl>720</ttl>
			<image>
				<url>http://www.watermark.org/favicon.ico</url>
				<title>Watermark Audio</title>
				<link>http://www.watermark.org/</link>
			</image>

			<xsl:apply-templates />
		</channel>
	</rss>
</xsl:template>


<xsl:template match="title[parent::head]">
	<title><xsl:value-of select="." /></title>
	<description><xsl:value-of select="." /></description>
</xsl:template>


<!-- button class="message-action btn" id="message-lisen-desktop" data-action='audio' data-audio='[{&quot;file&quot;:&quot;https:\/\/s3.amazonaws.com\/Wccaudio\/20200209_dal.mp3&quot;}]' data-target='m_player'> ... </button> -->

<xsl:template match="button[@data-audio and contains(@data-audio, '.mp3') and ancestor::div[contains(@class, 'desktop')]]">
	<xsl:variable name="media" select="translate(concat('http', substring-after(substring-before(@data-audio, '.mp3'), 'http'), '.mp3'), '\', '')" />

	<xsl:variable name="dt-str" select="substring(translate($media, 'https:\/\/s3.amazonaws.com\/Wccaudio\/', ''), 1, 8)" /><!-- e.g. 20200209 -->
	<xsl:variable name="dt" select="concat(substring($dt-str, 1, 4), '-', substring($dt-str, 5, 2), '-', substring($dt-str, 7, 2), 'T12:00:00-06:00')" />

	<!-- a href="https://www.watermark.org/person/david-marvin">David Marvin</a -->
	<xsl:variable name="creator" select="/html/body//a[contains(@href, '/person')][1]" />

	<item>
		<title><xsl:text>podcast </xsl:text><xsl:value-of select="$media" /></title>
		<guid><xsl:value-of select="$media" /></guid>
		<link><xsl:value-of select="$media" /></link>
		<enclosure url="{$media}" length="" type="audio/mpeg" />
		<pubDate>
			<xsl:call-template name="format-pubDate"><xsl:with-param name="dt" select="$dt" /></xsl:call-template>
		</pubDate>
		<dc:creator><xsl:value-of select="$creator" /></dc:creator>

		<description><xsl:text>podcast </xsl:text><xsl:value-of select="$media" /></description>
		<category>sermons</category>
		<category>messages</category>
		<category><xsl:value-of select="translate($creator, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" /></category>
	</item>
</xsl:template>


<!--
<a class="card a-block message-card " href="/message/3159-kara-s-testimony-on-fear-anger-and-people-pleasing">
	<img class="card-img-top" src="https://d170ij4dxqgjgp.cloudfront.net/assets/20190812/d0a75eb4-62eb-4850-9cf7-002d7d229648/Regen-STORIES-WEBSITE.jpg" alt="Kara's Testimony on Fear, Anger and People Pleasing"/>
	<div class="card-body">
		<h5 class="card-title">Kara's Testimony on Fear, Anger and People Pleasing</h5>
		<p class="card-text media-caption"><span class="module-media-show__speaker">Kara, re:generation</span> &#x2022; <span class="module-media-show__date">Nov 17, 2014</span>
		 </p>
	</div>
</a>

wget -SO - 'https://media.watermark.org/embeds/3159/audio?theme=watermark'
-->

<xsl:variable name="sQ">'</xsl:variable>

<vars:months>
	<vars:month value="01">Jan</vars:month>
	<vars:month value="02">Feb</vars:month>
	<vars:month value="03">Mar</vars:month>
	<vars:month value="04">Apr</vars:month>
	<vars:month value="05">May</vars:month>
	<vars:month value="06">Jun</vars:month>
	<vars:month value="07">Jul</vars:month>
	<vars:month value="08">Aug</vars:month>
	<vars:month value="09">Sep</vars:month>
	<vars:month value="10">Oct</vars:month>
	<vars:month value="11">Nov</vars:month>
	<vars:month value="12">Dec</vars:month>
</vars:months>


<!-- Elide too many testimonies, don't want to hit their servers too hard and more than 20 in one week is unlikely. -->

<xsl:template priority="0.6" match="div[
	'card-body' = @class
	and parent::a[contains(@class, 'message-card')]
	and count(parent::a/parent::div['col-10' = @class]/preceding-sibling::div['col-10' = @class]) >= 20
]" />


<xsl:template match="div['card-body' = @class and parent::a[contains(@class, 'message-card')]]">
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="parent::a/@href" /></xsl:call-template></xsl:variable>

	<xsl:variable name="dt-span" select="p[contains(@class, 'media-caption')]/span[contains(@class, 'date')]" /> <!-- e.g. Nov 17, 2014 -->
	<xsl:variable name="mn-name" select="substring($dt-span, 1, 3)" />
	<xsl:variable name="mn" select="1 + count(document('')/*/vars:months/vars:month[. = $mn-name]/preceding-sibling::vars:month)" />
	<xsl:variable name="dy" select="number(substring-before(substring-after($dt-span, ' '), ','))" />
	<xsl:variable name="yr" select="number(substring-after($dt-span, ', '))" />

	<xsl:variable name="dt" select="concat(str:align($yr, '0000', 'right'), '-', str:align($mn, '00', 'right'), '-', str:align($dy, '00', 'right'), 'T12:00:00-06:00')" />
	<xsl:variable name="pubDate"><xsl:call-template name="format-pubDate"><xsl:with-param name="dt" select="$dt" /></xsl:call-template></xsl:variable>

	<xsl:variable name="_creator" select="translate(substring-before(h5['card-title' = @class], ' '), '-', '')" />
	<xsl:variable name="creator">
		<xsl:choose>
			<xsl:when test="contains($_creator, concat($sQ, 's'))"><xsl:value-of select="substring-before($_creator, concat($sQ, 's'))" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="$_creator" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<item>
		<title>
			<xsl:value-of select="h5" />
			<xsl:text> [</xsl:text>
			<xsl:value-of select="substring-before($pubDate, ' 12:00')" />
			<xsl:text>]</xsl:text>
		</title>
		<guid><xsl:value-of select="$href" /></guid>
		<link><xsl:value-of select="$href" /></link>
		<pubDate><xsl:value-of select="$pubDate" /></pubDate>
		<dc:creator><xsl:value-of select="$creator" /></dc:creator>

		<description><xsl:text>testimony </xsl:text><xsl:value-of select="$href" /></description>
		<category>testimony</category>
		<category>recovery</category>

		<xsl:for-each select="str:tokenize(translate(h5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), concat($sQ, '&amp;-,/'))">
			<xsl:variable name="cat">
				<xsl:for-each select="str:tokenize(., ' ')">
					<xsl:if test="not('s' = . or 'and' = . or 'on' = . or 'of' = . or 'past' = . or 'low' = . or 'lack' = . or 'testimony' = .)">
						<xsl:value-of select="." />
						<xsl:text> </xsl:text>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<xsl:if test="normalize-space($cat)"><category><xsl:value-of select="normalize-space($cat)" /></category></xsl:if>
		</xsl:for-each>
	</item>
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="text()" />


</xsl:stylesheet>
