<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:str="http://exslt.org/strings"
	exclude-result-prefixes="str"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />

<xsl:param name="url" select="'http://e.g.url/'" />
<!-- xsl:variable name=search_url" select="'https://duckduckgo.com/html?q='" / -->
<xsl:variable name="search_url" select="'https://www.startpage.com/do/dsearch?pl=opensearch&amp;language=english&amp;query='" />


<xsl:template name="resolve-uri">
	<xsl:param name="href" />
	<xsl:variable name="toks" select="str:tokenize($url, '/')" />
		<!-- Toks: 1=http: 2=www.wayback.com 3=path1 4=path2 5=... -->
	<xsl:variable name="base-href" select="concat($toks[1], '//', $toks[2])" />

	<xsl:choose>
		<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
		<xsl:when test="starts-with($href, '/')"><xsl:value-of select="concat($base-href, $href)" /></xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="'/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, $href)" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($url, '/', $href)" /></xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="html">
	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<link><xsl:value-of select="$url" /></link>
			<xsl:apply-templates />
		</channel>
	</rss>
</xsl:template>


<xsl:template match="title[parent::head]">
	<title><xsl:value-of select="." /></title>
</xsl:template>


<xsl:template match="h1['theatreName' = @class and parent::div['print' = @class]]">
	<description><xsl:value-of select="." /></description>
</xsl:template>


<xsl:template match="link[parent::head and 'shortcut icon' = @rel and @href]">
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="@href" /></xsl:call-template></xsl:variable>

	<image>
		<url><xsl:value-of select="$href" /></url>
		<title><xsl:text>Movies @ </xsl:text><xsl:value-of select="$url" /></title>
		<link><xsl:value-of select="$url" /></link>
	</image>
</xsl:template>


<xsl:template match="div['showtimesInner' = @id]">
	<xsl:variable name="showdate" select="preceding-sibling::h1['print' = @class and contains(., 'Showtimes for')]" />

	<item>
		<guid><xsl:value-of select="$url" />/<xsl:value-of select="translate($showdate, ', ', '')" /></guid>
		<link><xsl:value-of select="$url" /></link>
		<title><xsl:value-of select="$showdate" /></title>
		<description>
			<div>
				<dl title="cinemark movie">
					<xsl:apply-templates />
				</dl>
			</div>
		</description>
		<dc:creator><xsl:value-of select="/html/body//h1['theatreName' = @class][1]" /></dc:creator>
	</item>
</xsl:template>


<xsl:template match="div[starts-with(@class, 'showtimeMovieBlock')]">
	<xsl:variable name="movie-title" select=".//h3" />
	<xsl:variable name="movie-title-uri" select="translate($movie-title, ' ()', '+')" />
	<xsl:variable name="poster-img" select="(.//img[contains(@data-srcset, 'poster') or contains(@alt, 'Poster')])[1]" />
	<xsl:variable name="img-href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="($poster-img/@data-srcset|$poster-img/@data-src)[1]" /></xsl:call-template></xsl:variable>

	<dt>
		<img src="{$img-href}"><xsl:apply-templates select="$poster-img/@*" /></img>
		<a href="{$search_url}'rottentomatoes+movie+reviews'+'{$movie-title-uri}'" alt="{$movie-title}"><xsl:value-of select="$movie-title" /></a>
	</dt>
	<dd>
		<xsl:apply-templates />
	</dd>
</xsl:template>


<xsl:template match="div['showtimePrintType' = @class]">
	<dd>
		<strong>
			<xsl:text>[</xsl:text>
			<xsl:for-each select=".//img['access' != @class]">
				<xsl:if test="position() > 1"><xsl:text>, </xsl:text></xsl:if>
				<xsl:value-of select="./@class" />
			</xsl:for-each>

			<xsl:if test="not(.//img['access' != @class])">
				<xsl:value-of select="h4/text()|.//img['access' = @class]/@alt" />
			</xsl:if>
			<xsl:text>]</xsl:text>
		</strong>
		<xsl:apply-templates select="following-sibling::div[starts-with(@class, 'showtimeMovieTimes')][1]/div/div['showtime' = @class]" />
	</dd>
</xsl:template>


<!-- Do not traverse normally, already displayed w/ the theater types above. -->
<xsl:template match="div[starts-with(@class, 'showtimeMovieTimes')]" />


<!-- div class="showtime" role="listitem"><a href="...">11:20am</a></div -->
<!-- Display the times in a bar (|) delimited list -->

<xsl:template match="div['showtime' = @class]">
	<xsl:if test="position() > 1">
		<xsl:text> |</xsl:text>
	</xsl:if>
	<xsl:text> </xsl:text>
	<xsl:value-of select="normalize-space(a|p)" />
</xsl:template>


<xsl:template match="@*">
	<xsl:copy />
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="text()" />


</xsl:stylesheet>
