<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:lib="http://127.0.0.1/functions"
	exclude-result-prefixes="lib"
>
<!-- xsl:include href="http://127.0.0.1/tt-rss/plugins.local/xslt/xsl/library.xsl" / -->
<xsl:include href="library.xsl" />
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />


<xsl:template match="html">
	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<link>http://www.example.com/</link>
			<description>http://www.example.com/ | Anchors</description>
			<title>Podcast Title</title>
			<item>
				<guid>http://www.example.com/article1</guid>
				<link>http://www.example.com/article1</link>
				<title>Article Title 1</title>
				<dc:creator>author1</dc:creator>
				<description>
					Article Title 1 [http://www.example.com/article1]
					--
					!0=<!-- xsl:value-of select="lib:resolve-uri('http://www.example.com/', '/article2/index.html')" / -->!
					!1=<xsl:copy-of select="document('')/*" />!
					!2=<xsl:copy-of select="document('library.xsl')/*" />!
					!3=<xsl:copy-of select="document('feedmorph.xsl')/*" />!
				</description>
			</item>
		</channel>
	</rss>
</xsl:template>


</xsl:stylesheet>
