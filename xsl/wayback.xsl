<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:php="http://php.net/xsl"
	xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	exclude-result-prefixes="php str date"
>
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="no" />

<xsl:param name="url" select="'https://www.gocomics.com/calvinandhobbes/~Y/~m/~d'" />
<xsl:param name="uniqid" select="'Uniquely identifies this processor when calling back into php.'" />
<xsl:param name="author" />
<xsl:param name="add" />
<xsl:param name="dow" />

<xsl:variable name="now">
	<xsl:choose>
		<xsl:when test="contains($add, 'P')"><xsl:value-of select="date:add(date:date-time(), $add)" /></xsl:when>
		<xsl:otherwise><xsl:value-of select="date:date-time()" /></xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="link" select="
string(
	str:replace(
		str:replace(
			str:replace(
				str:replace(
					str:replace($url, '~Y', date:year($now))
				, '~m', str:align(date:month-in-year($now), '00', 'right'))
			, '~d', str:align(date:day-in-month($now), '00', 'right'))
		, '~y', str:align(date:year($now) mod 100, '00', 'right'))
	, '~j', translate(substring-before(date:difference($now, $add), 'D'), '-P', ''))
)" />


<xsl:template name="resolve-uri">
	<xsl:param name="href" />
	<xsl:variable name="toks" select="str:tokenize($url, '/')" />
		<!-- Toks: 1=http: 2=www.wayback.com 3=path1 4=path2 5=... -->
	<xsl:variable name="base-href" select="concat($toks[1], '//', $toks[2])" />

	<xsl:choose>
		<xsl:when test="contains($href, '://') or starts-with($href, '//')"><xsl:value-of select="$href" /></xsl:when>
		<xsl:when test="starts-with($href, '/')"><xsl:value-of select="concat($base-href, $href)" /></xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="'/' = substring($url, string-length($url), 1)"><xsl:value-of select="concat($url, $href)" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($url, '/', $href)" /></xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="root">
	<!-- xsl:message>
		date:date()=<xsl:value-of select="date:date()" />
		?=<xsl:value-of select="substring-after($url, '?')" />
		date:difference()=<xsl:value-of select="date:difference(date:date(), substring-after($url, '?'))" />
	</xsl:message -->

	<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
		<channel>
			<xsl:if test="not($dow) or $dow = date:day-name($now)">
				<xsl:choose>
					<xsl:when test="contains($link, '.gif')">
						<!-- Not a parseable document, just emit some rss -->
						<xsl:call-template name="emit-rss" />
					</xsl:when>

					<xsl:otherwise>
						<xsl:call-template name="process-html">
							<xsl:with-param name="doc" select="php:function('url2dom', $uniqid, $link)" />
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>

			</xsl:if>

		</channel>
	</rss>
</xsl:template>


<xsl:template name="emit-rss">
	<title><xsl:value-of select="$link" /></title>
	<description><xsl:value-of select="$link" /></description>

	<item>
		<guid><xsl:value-of select="$link" /></guid>
		<link><xsl:value-of select="$link" /></link>
		<title>
			<xsl:value-of select="date:day-name($now)" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="date:month-name($now)" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="date:day-in-month($now)" />
			<xsl:text>, </xsl:text>
			<xsl:value-of select="date:year($now)" />
		</title>

		<description>
			<img title="{$link} comic" src="{$link}" />
		</description>
		<xsl:if test="$author">
			<dc:creator><xsl:value-of select="$author" /></dc:creator>
		</xsl:if>
	</item>

</xsl:template>


<xsl:template name="process-html">
	<xsl:param name="doc" />

	<xsl:apply-templates select="$doc" mode="channel" />

	<xsl:if test="not($doc/html/head/link['icon' = @rel])">
		<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="'/favicon.ico'" /></xsl:call-template></xsl:variable>
		<image>
			<url><xsl:value-of select="$href" /></url>
			<title><xsl:text>Fav Icon</xsl:text></title>
			<link><xsl:value-of select="$link" /></link>
		</image>
	</xsl:if>

	<item>
		<guid><xsl:value-of select="$link" /></guid>
		<link><xsl:value-of select="$link" /></link>
		<title>
			<xsl:value-of select="date:day-name($now)" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="date:month-name($now)" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="date:day-in-month($now)" />
			<xsl:text>, </xsl:text>
			<xsl:value-of select="date:year($now)" />
		</title>

		<description>
			<div>
				<xsl:apply-templates select="$doc" mode="description" />
			</div>
		</description>

		<xsl:apply-templates select="$doc" mode="item" />
		<xsl:if test="$author">
			<dc:creator><xsl:value-of select="$author" /></dc:creator>
		</xsl:if>
	</item>

</xsl:template>


<xsl:template match="meta[parent::head and 'og:url' = @property and @content]" mode="channel">
	<link><xsl:value-of select="@content" /></link>
</xsl:template>


<xsl:template match="title[parent::head]" mode="channel">
	<title><xsl:value-of select="normalize-space(.)" /></title>
	<description><xsl:value-of select="normalize-space(.)" /></description>
</xsl:template>


<xsl:template match="link[parent::head and 'icon' = @rel and not(preceding-sibling::link[parent::head and 'icon' = @rel])]" mode="channel">
	<xsl:variable name="href"><xsl:call-template name="resolve-uri"><xsl:with-param name="href" select="@href" /></xsl:call-template></xsl:variable>

	<image>
		<url><xsl:value-of select="$href" /></url>
		<title><xsl:text>Icon of </xsl:text><xsl:value-of select="@sizes" /></title>
		<link><xsl:value-of select="../meta['og:url' = @property]/@content" /></link>
	</image>
</xsl:template>


<!-- meta property="article:author" content="Bill Watterson" / -->

<xsl:template match="meta[parent::head and 'article:author' = @property]" mode="item">
	<dc:creator><xsl:value-of select="@content" /></dc:creator>
</xsl:template>


<!-- div class="author"><p>Jim Toomey</p></div -->

<xsl:template match="div['author' = @class and child::p]" mode="item">
	<dc:creator><xsl:value-of select="child::p" /></dc:creator>
</xsl:template>


<xsl:template match="meta['og:image' = @property and @content and not(contains(@content, 'social_images'))]" mode="description">
	<img title="{$link} comic" src="{@content}" />
</xsl:template>


<!-- These 2 are the far side -->

<xsl:template match="div['card-body' = @class]" mode="description">
	<xsl:apply-templates mode="description" />
	<hr />
</xsl:template>


<xsl:template match="img[@data-src and ancestor::div[contains(@class, 'js-daily-dose')]]" mode="description">
	<p><img title="{$link} comic" src="{@data-src}" /></p>
</xsl:template>


<xsl:template match="figcaption['figure-caption' = @class and ancestor::div[contains(@class, 'js-daily-dose')]]" mode="description">
	<p><xsl:value-of select="normalize-space(.)" /></p>
</xsl:template>


<!-- meta property="article:tag" content="business,interview,questions,job market,engineers,baker,mortuary,assistant"/ -->

<xsl:template match="meta['article:tag' = @property and @content]" mode="description">
	<xsl:for-each select="str:tokenize(@content, ',&#x0A;&#x2060;&#x2800;')">
		<category><xsl:value-of select="." /></category>
	</xsl:for-each>
</xsl:template>


<!-- Elide noise ... -->

<xsl:template match="text()" />
<xsl:template match="text()" mode="channel" />
<xsl:template match="text()" mode="item" />
<xsl:template match="text()" mode="description" />


</xsl:stylesheet>
