<?php

/* Copy this file to 'data/.appauth.php' and customize the usernames and passwords. */

global $profile_auths;
$profile_auths = array(
	'instagram.accountName1' => array('id' => 'username1', 'pwd' => 'password1'),
	'instagram.accountName2' => array('id' => 'username2', 'pwd' => 'password2'),
	'instagram.ALL' => array('id' => 'username-foreverything', 'pwd' => 'password-foreverything')
);
